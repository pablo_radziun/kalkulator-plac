
Użytkownik, aby sprawdzić potencjalne wynagrodzenie musi wypełnić dwa formularze:
1. Podstawowe dane:
•	Rodzaj umowy - pole wielokrotnego wyboru:
o	Umowa o pracę
o	Umowa o dzieło
o	Umowa zlecenie

•	Rok - pole numeryczne: rok w którym wypłacane ma być wynagrodzenie (domyślnie
•	aktualny rok)
•	kwota brutto/netto – radio button
•	kwota wynagrodzenia

2. Dane szczegółowe umowy. W zależności od wybranego rodzaju umowy:
•	Umowa o pracę – użytkownik od razu jest przekierowany na stronę z tabelą wypłat
•	Umowa zlecenie – użytkownik musi dodatkowo podać informacje, czy dodatkowo są odprowadzane skłądki:
o	Składka rentowa – tak/nie radio button
o	Składka emerytalna – tak/nie radio button
o	Składka chorobowa– tak/nie radio button
o	Składka zdrowotna – tak/nie radio button
o	Koszty uzyskania przychodu – 20%/50% radio buton
•	Umowa o dzieło – użytkownik musi dodatkowo określić koszty uzyskania przychodu:
o	Koszty uzyskania przychodu – 20%/50% radio button
Po wypełnieniu formularzy, generowana jest tabelka z poszczególnymi wypłatami.