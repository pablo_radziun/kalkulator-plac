<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator umowy</title>
</head>
<body>

<jsp:useBean id="parameters" class="domain.ContractUsualData" scope="session"/>
<jsp:setProperty property="*" name="parameters"/>

<c:if test="${parameters.getContractType() eq 'employment'}" >
	<c:redirect url="result.jsp"/>
</c:if>

<c:if test="${parameters.getContractType() eq 'work'}" >
	<c:redirect url="work.jsp"/>
</c:if>

<c:if test="${parameters.getContractType() eq 'order'}" >
	<c:redirect url="order.jsp"/>
</c:if>

</body>
</html>