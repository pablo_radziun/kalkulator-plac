<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator umów</title>
</head>
<body>
	<jsp:useBean id="parameters" class="domain.ContractUsualData" scope="session"/>
	<jsp:useBean id="salaryService" class="service.SalaryService" scope="application"/>
	<jsp:setProperty property="*" name="parameters"/>
	<jsp:setProperty property="*" name="salaryService"/>

<c:if test="${parameters.contractType eq 'employment'}">
		<h3>Dane dla umowy o pracę</h3>
		<%= salaryService.calculateForEmployment(parameters) %>
		
	</c:if>
	<c:if test="${parameters.contractType eq 'order'}">
		<jsp:useBean id="orderData" class="domain.Order"scope="session"/>
		<jsp:setProperty property="*" name="orderData"/>
		<h3>Dane dla umowy zlecenia</h3>
		<%= salaryService.calculateForOrder(parameters, orderData) %>
	</c:if>
	<c:if test="${parameters.contractType eq 'work'}">
		<jsp:useBean id="workData" class="domain.Work" scope="session"/>
		<jsp:setProperty property="*" name="workData"/>
		<h3>Dane dla umowy o dzieło</h3>
		<%= salaryService.calculateForWork(parameters, workData) %>
	</c:if>
	
	
</body>
</html>