<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kalkulator umowy</title>
</head>
<body>

	<h2><b>Wprowadź dane</b></h2><br/>
	<form action="intermediary.jsp">
	Rodzaj umowy:&nbsp;
	<select name="contractType">
		<option value="employment">o pracę</option>
		<option value="work">o dzieło</option>
		<option value="order">zlecenie</option>
	</select><br/>
	Rok obliczanego wynagrodzenia:&nbsp;
	<input type="number" name="year" min="2016" step="1" max="2030" value="2016"/>
	<br/>
	Kwota:&nbsp
	<input type="radio" name="netbt" value="brutto" checked/>brutto&nbsp;
	<input type="radio" name="netbt" value="netto"/>netto<br/>
	Kwota wynagrodzenia:&nbsp;
	<input type="number" name="salary" min="0" step="50" max="150000"/><br/>
	<input type="submit" name="action" value="wyślij"/>
	<input type="reset" value="wyczyść"/>
	</form>
	
</body>
</html>