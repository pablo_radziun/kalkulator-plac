package domain;

public class ContractUsualData {
	private String contractType;
	private int year;
	private String netbt;
	private int salary;
	
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getNetbt() {
		return netbt;
	}
	public void setNetbt(String netbt) {
		this.netbt = netbt;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
}
