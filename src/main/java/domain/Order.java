package domain;

public class Order {
	String pension;
	String retiring;
	String sickness;
	String incomecosts;
	public String getPension() {
		return pension;
	}
	public void setPension(String pension) {
		this.pension = pension;
	}
	public String getRetiring() {
		return retiring;
	}
	public void setRetiring(String retiring) {
		this.retiring = retiring;
	}
	public String getSickness() {
		return sickness;
	}
	public void setSickness(String sickness) {
		this.sickness = sickness;
	}
	public String getIncomecosts() {
		return incomecosts;
	}
	public void setIncomecosts(String incomecosts) {
		this.incomecosts = incomecosts;
	}

	
}
