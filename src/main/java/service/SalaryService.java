package service;


import domain.*;


public class SalaryService {

	ContractUsualData cUData = new ContractUsualData();
	Order order = new Order();
	Work work = new Work();
	String show;
	String[] month = {"styczeń", "luty", "marzec", "kwiecień", "maj", 
			"czerwiec", "lipiec", "sierpień", "wrzesień", "październik", 
			"lstopad", "grudzień"};
	double a, c, d, e, f, g, h, i, j, k, retiring, pension, sickness, health, incomeCost;
	
	public String calculateForEmployment(ContractUsualData cUsualData){
		this.cUData = cUsualData;
		rates();
		if (cUData.getNetbt().equals("brutto")){
			g = 111.25;
			h = (double)Math.round(a-c-g);
			i = (double)(Math.round((0.18*h - 46.33)*100))/100;
			j = (double)Math.round(i-f);
			k = (double)(Math.round((a-c-e-j)*100))/100;
			show = "<table border=\"1\" cellpadding=\"7px\" "
				+ "style=\"width:50%; text-align:center; border-collapse:collapse;\" >"
				+ "<tr><th rowspan=\"2\" ></th>"
				+ "<th rowspan=\"2\" >Brutto</th>"
                + "<td colspan=\"4\">Ubezpieczenie</td>"
                + "<td rowspan=\"2\" >Podstawa opodatkowania</td>"
                + "<td rowspan=\"2\" >Zaliczka na PIT</td>"
                + "<th rowspan=\"2\" >Netto</th></tr>"
                + "<td>Emerytalne</td><td>Rentowe</td><td>Chorobowe</td>"
				+ "<td>Zdrowotne</td>";
			for (int i=0; i<12 ; i++){
				show += "<tr><th>" + month[i] + "</th><th>" + a + "</th>"
					+ "<td>" + retiring + "</td><td>" + pension + "</td>"
					+ "<td>" + sickness + "</td><td>" + health + "</td>"
                    + "<td>" + h + "</td><td>" + j + "</td><th>" + k + "</th></tr>";
		}
			show += "<tr><th>suma</th><th>" 
					+ (double)(Math.round(12*a*100))/100 + "</th><th>" 
					+ (double)(Math.round(12*retiring*100))/100 + "</th><th>" 
					+ (double)(Math.round(12*pension*100))/100 + "</th><th>" 
					+ (double)(Math.round(12*sickness*100))/100 + "</th><th>" 
					+ (double)(Math.round(12*health*100))/100 + "</th><th>" 
					+ (double)(Math.round(12*h*100))/100 + "</th><th>" 
					+ (double)(Math.round(12*j*100))/100 + "</th><th>" 
					+ (double)(Math.round(12*k*100))/100 + "</th></tr>";
		}
		if (cUsualData.getNetbt().equals("netto")){
			show = "<table border=\"1\" cellpadding=\"7px\" "
					+ "style=\"text-align:center; border-collapse:collapse;\" >"
					+ "<tr><td></td><th>Netto</th></tr>";
			for (int i=0; i<12 ; i++){
				show += "<tr><th>" + month[i] + "</th><th>" + a + "</th></tr>";
			}
			show += "<tr><th>suma</th><th>"	+ (double)(Math.round(12*a*100))/100 
					+ "</th></tr>"; 
		}
		return show;
	}
	
	public String calculateForOrder(ContractUsualData cUsualData, Order order){
		this.cUData = cUsualData;
		this.order = order;
		rates();
		if (cUData.getNetbt().equals("brutto")){
			if (order.getIncomecosts().equals("20"))
				incomeCost = (double)(Math.round(0.2*d*100))/100;
			if (order.getIncomecosts().equals("50"))
				incomeCost = (double)(Math.round(0.5*d*100))/100;
				g = incomeCost;
				h = (double)Math.round(a-c-g);
				i = (double)(Math.round(0.18*h*100))/100;
				j = (double)Math.round(i-f);
				k = (double)(Math.round((a-c-e-j)*100))/100;
				show = "<table border=\"1\" cellpadding=\"7px\" "
						+ "style=\"width:50%; text-align:center; border-collapse:collapse;\" >"
						+ "<tr><th rowspan=\"2\" >Brutto</th>"
						+ "<td colspan=\"4\">Ubezpieczenie</td>"
						+ "<td rowspan=\"2\" >Koszt uzyskania przychodu</td>"
						+ "<td rowspan=\"2\" >Podstawa opodatkowania</td>"
						+ "<td rowspan=\"2\" >Zaliczka na PIT</td>"
						+ "<th rowspan=\"2\" >Netto</th></tr>"
						+ "<td>Emerytalne</td><td>Rentowe</td><td>Chorobowe</td>"
						+ "<td>Zdrowotne</td>"
						+ "<tr><th>" + a + "</th>"
						+ "<td>" + retiring + "</td><td>" + pension + "</td>"
						+ "<td>" + sickness + "</td><td>" + health + "</td><td>" 
						+ incomeCost + "</td><td>" + h + "</td><td>" + j + "</td><th>" 
						+ k + "</th></tr>";
		}
		if (cUData.getNetbt().equals("netto")){
			show = "<table border=\"1\" cellpadding=\"7px\" "
					+ "style=\"width:50%; text-align:center; border-collapse:collapse;\" >"
					+ "<tr><th>Netto</th></tr>"
					+ "<tr><th>" + a + "</th></tr>";
		}
		return show;
	}
	
	public String calculateForWork(ContractUsualData cUsualData, Work work){
		this.cUData = cUsualData;
		this.work = work;
		a = cUData.getSalary();
		if (cUData.getNetbt().equals("brutto")){
			if (work.getIncomecosts().equals("20"))
				incomeCost = (double)(Math.round(0.2*a*100))/100;
			if (work.getIncomecosts().equals("50"))
				incomeCost = (double)(Math.round(0.5*a*100))/100;
				i = (double)(Math.round((a-incomeCost)*100))/100;
				j = (double)(Math.round(0.18*i*100))/100;
				k = (double)(Math.round((a-j)*100))/100;
				show = "<table border=\"1\" cellpadding=\"7px\" "
						+ "style=\"width:50%; text-align:center; border-collapse:collapse;\" >"
						+ "<tr><th>Brutto</th>"
						+ "<td>Koszt uzyskania przychodu</td>"
						+ "<td>Podstawa opodatkowania</td>"
						+ "<td>Zaliczka na PIT</td>"
						+ "<th>Netto</th></tr>"
						+ "<tr><th>" + a + "</th><td>"
						+ incomeCost + "</td><td>"
						+ i + "</td><td>"
						+ j + "</td><td>"
						+ k + "</td></tr>";
			}
			if (cUData.getNetbt().equals("netto")){
				show = "<table border=\"1\" cellpadding=\"7px\" "
					+ "style=\"width:50%; text-align:center; border-collapse:collapse;\" >"
					+ "<tr><th>Netto</th></tr>"
					+ "<tr><th>" + a + "</th></tr>";
			}
		return show;
	}
	
	public void rates(){
		a = cUData.getSalary();
		if (cUData.getContractType().equals("employment") || order.getRetiring().equals("yes")) 
			retiring = (double)(Math.round(0.0976*a*100))/100;
		else retiring = 0;
		if (cUData.getContractType().equals("employment") || order.getPension().equals("yes")) 
			pension = (double)(Math.round(0.015*a*100))/100;
		else pension = 0;
		if (cUData.getContractType().equals("employment") || order.getSickness().equals("yes"))
			sickness = (double)(Math.round(0.0245*a*100))/100;
		else sickness = 0;
		c = retiring*a + pension*a + sickness*a;
		d = a - c;
		health = (double)(Math.round(0.09*d*100))/100;
		e = 0.09*d;
		f = 0.0775*d;
		g = 111.25;
		h = (int)Math.round(a-c-g);
		i = Math.round(0.18*h - 46.33);
		j = (int)Math.round(i-f);
		k = (double)(Math.round((a-c-e-j)*100))/100;
	}
}
